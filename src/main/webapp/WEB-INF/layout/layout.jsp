<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

	<head>
	
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<title>INSPINIA | Layouts  </title>
		
		<!-- Mainly scripts -->
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/jquery-3.1.1.min.js"/>"></script>
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/popper.min.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/bootstrap.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/dataTables/datatables.min.js"/>"></script>
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/sweetalert/sweetalert.min.js"/>"></script>
		
		<!-- Custom and plugin javascript -->
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/inspinia.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/pace/pace.min.js"/>"></script>
		<script	src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
		
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/bootstrap.min.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/animate.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/style.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/dataTables/datatables.min.css"/>" rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/sweetalert/sweetalert.css"/>"  rel="stylesheet">
    	
	
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav metismenu" id="side-menu">
						<li class="nav-header">
							<div class="dropdown profile-element">
								<img alt="image" class="img-thumbnail" src="<c:url value="/profile/userPhoto/${User.image}"/>" />
								<a data-toggle="dropdown" class="dropdown-toggle" href="table_data_tables.html#">
                            		<span class="block m-t-xs font-bold">${User.name}</span>
                            		<span class="text-muted text-xs block">設定 <b class="caret"></b></span>
                        		</a>
		                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
		                            <li><a class="dropdown-item" href="<c:url value="/profile/editAccount/${User.username}"/>">個人資料</a></li>
		                            <li class="dropdown-divider"></li>
		                            <li><a class="dropdown-item" href="<c:url value="/doLogout"/>">登出</a></li>
		                        </ul>
							</div>
						</li>
						<li class="">
							<a href="<c:url value="/profile"/>"><i class="fa fa-users"></i> <span class="nav-label">人員資料管理</span></a>
						</li>
						<li class="">
							<a href="<c:url value="/photoUpload"/>"><i class="fa fa-folder-open"></i> <span class="nav-label">上傳圖檔</span></a>
						</li>
						<li class="">
							<a href="<c:url value="/excel"/>"><i class="fa fa-upload"></i> <span class="nav-label">匯入EXCEL</span></a>
						</li>
					</ul>
				</div>
			</nav>
			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<div class="navbar-header">
							<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="table_data_tables.html#"><i class="fa fa-bars"></i> </a>
						</div>
						<ul class="nav navbar-top-links navbar-right">
							<li>
								<a href="<c:url value="/doLogout"/>"> <i class="fa fa-sign-out"></i>登出</a>
							</li>
						</ul>
					</nav>
				</div>
				
				<sitemesh:write property="body" />

			</div>
		</div>
	</body>
</html>

