<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<body>

<style>



</style>

 <!-- Image cropper -->
    <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/cropper/cropper.min.js"/>"></script>
    <link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/cropper/cropper.min.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/validate/jquery.validate.min.js"/>"></script>

	<div class="row wrapper border-bottom white-bg page-heading">
		<div class="col-lg-10">
			<h2>人員資料管理</h2>
		</div>
	</div>
	<%-- <div class="wrapper wrapper-content">
		<div class="row animated  ">
			<div class="col-md-6">
				<div class="ibox ">
					<div class="ibox-title">
						<h5>人員資料</h5>
						<!-- <button type="button" class="btn btn-primary pull-fight ibox-tools" data-toggle="modal" data-target="#myModal">
                        	&nbsp;編輯&nbsp;
                        </button> -->
					</div>
					<div>
						<div class="ibox-content no-padding border-left-right "> 
							<img alt="image" class="img-fluid" src="<c:url value="/resources/INSPINIA-master-2.9.4/img/profile_big.jpg"/>" style="display:block; margin:auto;">
						</div>
						<div id ="profileImfo"class="ibox-content profile-content">
							<div class="form-group"><label><i class="fa fa-user mr-2"></i>名稱</label> <input  placeholder="請入名稱" class="form-control" value="${User.username}"></div>
	       					<div class="form-group"><label><i class="fa fa-phone mr-2"></i>手機</label> <input  placeholder="請入手機號碼" class="form-control" value="${User.phone}"></div>
				       		<div class="form-group"><label><i class="fa fa-envelope mr-2"></i>電子信箱</label> <input  placeholder="請輸入電子信箱" class="form-control" value="${User.email}"></div>
				       		<div class="form-group"><label><i class="fa fa-user mr-2"></i>關於我</label> <textarea  placeholder="請輸入自我介紹" class="form-control" >${User.imformation}</textarea></div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	</div>
</div> --%>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>新增人員</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="form_basic.html#" class="dropdown-item">Config option 1</a>
                                    </li>
                                    <li><a href="form_basic.html#" class="dropdown-item">Config option 2</a>
                                    </li>
                                </ul>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                        	<form role ="form" id="form" novalidate="novalidate">
	                        <div class=" form-group row">
	                        <label class="col-sm-2 col-form-label"><i class="fa fa-picture-o mr-2"></i>頭貼設置</label>
	                            <div class="col-md-5" >
	                                <div class="image-crop" >
	                                    <img id="userImage" src="<c:url value="/profile/userPhoto/${accountDetail.image}"/>" style = "height : 280px;">
	                                </div>
	                            </div>
	                            <div class="col-md-5">
	                                <h4>預覽頭貼畫面</h4>
	                                <div class="img-preview img-preview-sm  " style="border-style: groove;"></div>
	                                <h4>說明</h4>
	                                <p>
	                                    可以上傳圖片，選擇想要的區塊作為大頭貼
	                                </p>
	                                <div>
	                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
	                                        <input type="file" accept="image/*"  id="inputImage" style="display:none">
	                                    	上傳圖片
	                                    </label>
	                                </div>
	                                <a href="form_advanced.html" id="download" class="btn btn-primary">下載</a>
	                            </div>
	                        </div>
	                        <div class="hr-line-dashed"></div>
                            
                                <div class="form-group  row"><label class="col-sm-2 col-form-label"><i class="fa fa-shield mr-2"></i>帳號</label>
                                    <div class="col-sm-3"><input type="text" class="form-control" name="username" id="username" value="${accountDetail.username}"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label"><i class="fa fa-key mr-2"></i>密碼</label>
                                     <div class="col-sm-3"><input type="password" class="form-control" name="password" id="password" value="${accountDetail.password}"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label"><i class="fa fa-user mr-2"></i>名稱</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" name= "name" id="name" value="${accountDetail.name}"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label"><i class="fa fa-phone mr-2"></i>手機</label>
                                    <div class="col-sm-2"><input type="text" class="form-control" name= "phone" id="phone" value="${accountDetail.phone}"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label"><i class="fa fa-envelope mr-2"></i>電子郵件</label>

                                    <div class="col-sm-4"><input type="email"  class="form-control" name = "email" id="email" value="${accountDetail.email}" ></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row"><label class="col-sm-2 col-form-label"><i class="fa fa-info-circle mr-2"></i>關於我</label>

                                    <div class="col-sm-7"><textArea  class="form-control" id ="information" > ${accountDetail.information}</textArea></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button onClick="$(location).attr('href', '<c:url value="/profile"/>')" type ="button" class="btn btn-white btn-sm" >取消</button>
                                        <button class="btn btn-primary btn-sm" type="submit" >儲存</button>     
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



            
                
                    
                    
                        <!-- <div class="row">
                            <div class="col-md-6">
                                <div class="image-crop" >
                                    <img src="img/p3.jpg" style = "height : 280px;">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h4>預覽畫面</h4>
                                <div class="img-preview "></div>
                                <h4>說明</h4>
                                <p>
                                    可以上傳圖片，選擇想要的區塊作為大頭貼
                                </p>
                                <div>
                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                        <input type="file" accept="image/*" name="file" id="inputImage" style="display:none">
                                    	上傳圖片
                                    </label>
                                </div>
                                <a href="form_advanced.html" id="download" class="btn btn-primary">下載</a>
                            </div>
                        </div>
                     -->
                    
               
           
        


                        
<script>
$(function(){
	

	var $image = $(".image-crop > img");
    var $cropped = $($(".image-crop > img")).cropper({
        aspectRatio: 1,
        preview: ".img-preview",
        done: function(data) {
            console.log(data);
        }
    });


var $inputImage = $("#inputImage");
if (window.FileReader) {
    $inputImage.change(function() {
    	
        var fileReader = new FileReader(),
                files = this.files,
                file;

        if (!files.length) {
            return;
        }

        file = files[0];

        if (/^image\/\w+$/.test(file.type)) {
            fileReader.readAsDataURL(file);
            fileReader.onload = function () {
                $inputImage.val("");
                $image.cropper("reset", true).cropper("replace", this.result);
                
            };
        } else {
            showMessage("Please choose an image file.");
        }
    });
} else {
    $inputImage.addClass("hide");
}

$("#download").click(function (link) {
    link.target.href = $cropped.cropper('getCroppedCanvas', { width: 620, height: 520 }).toDataURL("image/png").replace("image/png", "application/octet-stream");
    link.target.download = 'cropped.png';
    
   // console.log(link);
});

$("#form").validate({
	ignore: ":hidden",
    rules: {
    	username:{
    		 required: true
    	},
        password: {
            required: true
        },
        phone: {
            required: true,
            number:true,
            digits:true,
            rangelength:[10,10], 
        },  
        email: {
            required: true,
            email: true
           
        },
        name: {
            required: true
        }
        
    },
	messages: {
		username:{
		      required: "請輸入帳號"     
		    },
		password:{
		      required: "請輸入密碼"     
		    },
		email: {
		      required: "請輸入電子郵件",
		      email : "請輸入正確電子郵件格式"
		},
		phone: {
		      required: "請輸入電話號碼",
		      number:"請輸入數字",
	          digits:"請勿輸入特殊符號",
	          rangelength:"電話號碼 須為10碼", 
		},
		name: {
	    	required: "請輸入名稱"
	    }
	},
    submitHandler: function(form,e) {
    	e.preventDefault(); 
    	var image = $cropped.cropper('getCroppedCanvas', { width: 620, height: 520 }).toDataURL("image/png");
    	
    	data = {};
    	
    	data.username = $('form [id = username]').val();
    	data.password = $('form [id = password]').val();
    	data.name = $('form [id = name]').val();
    	data.phone = $('form [id = phone]').val();
    	data.information = $('form [id = information]').val();
    	data.email = $('form [id = email]').val();
    	data.image = image;
    	console.log(data);
    	
    	var accountDetail = "${accountDetail}";
    	
    	var url = null;
    		
    	if(accountDetail == ''){
    		url = "<c:url value="/profile/doAccountCreate"/>";
    		
    	}else{
    		url = "<c:url value="/profile/doAccountSave"/>";	
    	}
    	
    	$.ajax({
    		url : url,
    		method : "POST",
    		contentType : "application/json",
    		data : JSON.stringify(data),
    		success : function(result){
    			console.log(result);
    			
    			if( result.code == '0000' ){
					swal({
		                title: "成功",
		                text: result.message,
		                type: "success"
		            }, function () {
		            	$(location).attr('href', '<c:url value="/profile"/>');
		            });		
				}else{
					swal({
		                title: "失敗",
		                text: result.message,
		                type: "error"
		            }, function () {
		            	
		            });		
				}
    			
    		},
    		error : function(){
    			
    		}
    		
    		
    	});
    	   
    	   return false; // prevent normal form posting
    	   
    	
        	
      }
});

});
</script>
</body>
</html>