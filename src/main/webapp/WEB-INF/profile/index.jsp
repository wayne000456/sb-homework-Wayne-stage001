<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<body>
	<!-- iCheck -->
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"/>"rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/iCheck/custom.css"/>" rel="stylesheet">
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/iCheck/icheck.min.js"/>"></script>
	
		<script type="text/javascript">
			$(function(){
				
				function userTable() {
					$.ajax({
						
						url:"<c:url value="/profile/userTable"/>",
						method:"POST",
						
						success:function(data){
							
							drawUserTable(data.data);
							
							$('.i-checks').iCheck({
			        	        checkboxClass: 'icheckbox_square-green',
			        	        radioClass: 'iradio_square-green',
			        	    }); 
							
							$('#userTable tbody').on('click', ' button ', function () {
								
								var table = $('#userTable').DataTable();
								var rowData =table.row( $(this).parent().parent()[0] ).data();
								var modal = $('#myModal .modal-body');
								modal.find('[name = username]').val(rowData.username);
								modal.find('[name = name]').val(rowData.name);
								modal.find('[name = phone]').val(rowData.phone);
								modal.find('[name = email]').val(rowData.email);
								modal.find('[name = information]').val(rowData.information);
								
						    });
							console.log($('#userTable [name = check]'));
							
							$('#userTable .iCheck-helper').click(function(){
								/* var check = $(this).prev().prop('checked'); */
								var tr = $(this).parents('tr')
								tr.toggleClass('selected');
							});
							
							
						},
						
						error:function(){
							console.log("error")
						}
						
					});	
				
				}
			
				function drawUserTable(data){
					
					$('#userTable').DataTable({
						language: {
				            lengthMenu: '一頁顯示 _MENU_ 筆',
				            zeroRecords: '搜尋無資料',
				            info: 'Showing page _PAGE_ of _PAGES_',
				            infoEmpty: '查無資料',
				            infoFiltered: '(filtered from _MAX_ total records)',
				            search: "搜尋:",
				            paginate: {
				                "first":      "第一頁",
				                "last":       "最後一頁",
				                "next":       "下一頁",
				                "previous":   "上一頁"
				            },
					    },
					    select: true,
					    destroy : true,
				        pageLength : 10,
				        responsive : true,
				        data : data,  
				        
				       	columns : [
				        	{ title : "選擇" ,render : function(){
				        		return '<label class=" i-checks"><input name = "check" type="checkbox" value="option1"> </label>' ;
				        	} },
				     	  /*  	{ title : "帳號" , "data" : "username" }, */
				        	{ title : "使用者名稱" , "data" : "name" },
				        	{ title : "手機號碼" , "data" : "phone" },
				        	{ title : "電子信箱" , "data" : "email" },
							{ title : "-" ,render : function(){
								 
								return '<button type="button" class="btn  btn-primary btn-sm  " data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> &nbsp;編輯&nbsp;	</button> ';
				        		//return '<button class="btn btn-info btn-sm " type="button"><i class="fa fa-edit"></i> 編輯 </button>' ;
				        		
				        	} },
				        ],					        
				     
				  	});
						
				}
				
				userTable();
				
				$('#userSaveBtn').click(function(){
					
					var data = {};
					var modal = $('#myModal .modal-body');
					
					data.username = modal.find('[name = username]').val();
					data.name = modal.find('[name = name]').val();
					data.phone = modal.find('[name = phone]').val();
					data.email = modal.find('[name = email]').val();
					data.information = modal.find('[name = information]').val();
					
					$.ajax({
						
						url : "<c:url value="/profile/doUserSave"/>",
						method : "POST",
						data : JSON.stringify(data),
						contentType : "application/json",
						
						success : function(result){
							
							console.log(result);
							
							if( result.code == '0000' ){
								
								swal({
					                title: "成功",
					                text: result.message,
					                type: "success"
					            }, function () {
					            	$('#myModal').modal('hide');
					            	$('#userTable').DataTable().clear().draw();
					            	userTable();
					            });
									
							}else{
								
								swal({
					                title: "失敗",
					                text: result.message,
					                type: "error"
					            });
								
							}	
								
						},
						
						error : function(error){
							console.log("error",error);
						}
	
					});
					
					
				});
				
					            	
				$('#userDelBtn').click(function(){
					
					var delUsersList = [];
					
					var table = $('#userTable').DataTable();
					var data = table.rows('.selected').data();
					
					for( var i =0 ; i < data.length ; i ++){
						delUsersList.push(data[i].username);
					}
					
					if( delUsersList.length < 1 ){
						
						swal({
			                title: "操作失敗",
			                text: "請勾選欲刪除人員",
			                type: "warning"
			            });	
						
					}else{
					
						swal({
			                title: "確定刪除資料?",
			                text: "共"+ " "+ delUsersList.length + " " +"筆資料" ,
			                type: "warning",
			               	showCancelButton: true,
			                confirmButtonColor: "#DD6B55",
			                confirmButtonText: "刪除",
			                cancelButtonText: "取消",
			                closeOnConfirm: false
			            }, function () {
			            	
			            	$.ajax({
			            		url : "<c:out value="profile/doUsersDelete"/>",
			            		method : "POST",
			            		data : JSON.stringify({"delUsersList":delUsersList}),
			            		contentType : "application/json",
			            		success : function(result){
			            			
			            			if( result.code == '0000' ){
										swal({
							                title: "成功",
							                text: result.message,
							                type: "success"
							            }, function () {
							            	$('#userTable').DataTable().clear().draw();
							            	userTable();
							            });		
									}else{
										swal({
							                title: "失敗",
							                text: result.message,
							                type: "error"
							            }, function () {
							            	$('#userTable').DataTable().clear().draw();
							            	userTable();
							            });		
									}
			            		
			            		},
			            		error : function(result){
			            			console.log(result);
			            		}
			            	});
			            	
			            });
						
					}
					
				});
				
				
				
					
			});
			
		</script>
		
		<div class="row wrapper border-bottom white-bg page-heading">
		    <div class="col-lg-10">
		        <h2>人員資料管理</h2>
		    </div>
		</div>
		<div class="wrapper wrapper-content animated fadeInRight"> 
		    <div class="row">
		        <div class="col-lg-12">    
	                <div class="ibox ">
	                
	                    <div class="ibox-title">
	                        <h5>人員資料表</h5>
	                    </div>
	                    <div class="ibox-tools" style="margin-right : 1%">
	                   		<button  id="userCreateBtn" class="btn btn-outline btn-success btn-sm" onclick="location='<c:url value="profile/createAccount"/>'" ><i class="fa fa-user-circle"></i> &nbsp;新增&nbsp;</button>
                        	<button  id="userDelBtn" class="btn btn-outline btn-danger btn-sm " ><i class="fa fa-trash"></i> &nbsp;刪除&nbsp;</button>
                        </div>
	                    
	                    <div class="ibox-content">
	                    	<div class="table-responsive">
								<table id ="userTable" class="table table-striped table-bordered table-hover " style="width:100%"  ></table>
							</div>
	                    </div>
	                </div>
		        </div>
		    </div> 
		</div>
		
		<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; ;  cursor: auto;" >
			<div class="modal-dialog">
			<div class="modal-content animated bounceInRight">
			        <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			            <i class="fa fa-user modal-icon"></i>
			            <h4 class="modal-title">編輯個人資料</h4>
			        </div>
			        <div class="modal-body">			  
			        	<!-- <input name = "username"  class="form-control" > -->
			       		<div class="form-group"><label><i class="fa fa-user mr-2"></i>名稱</label> <input name = "name"  placeholder="請入名稱" class="form-control"></div>
			       		<div class="form-group"><label><i class="fa fa-phone mr-2"></i>手機</label> <input name = "phone"  placeholder="請入手機號碼" class="form-control"></div>
			       		<div class="form-group"><label><i class="fa fa-envelope mr-2"></i>電子信箱</label> <input name = "email" placeholder="請輸入電子信箱" class="form-control"></div>
			       		<div class="form-group"><label><i class="fa fa-info-circle mr-2"></i>關於我</label> <textarea name = "information" placeholder="請輸入自我介紹" class="form-control"></textarea></div>		    
			        </div>
			        <div class="modal-footer">
			            <button type="button"  class="btn btn-white" data-dismiss="modal">關閉</button>
			            <button type="button" id="userSaveBtn" class="btn btn-primary">儲存</button>
			        </div>
			    </div>
			</div>
		</div>  
		 
	</body>
</html>