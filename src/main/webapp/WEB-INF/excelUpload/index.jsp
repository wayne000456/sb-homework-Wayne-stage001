<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<body>
	<!-- iCheck -->
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"/>"rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/iCheck/custom.css"/>" rel="stylesheet">
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/iCheck/icheck.min.js"/>"></script>
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/bs-custom-file/bs-custom-file-input.min.js"/>"></script>
	
		<script type="text/javascript">
			$(function(){
				

				bsCustomFileInput.init();
				
				
				$('#excelUploadBtn').click(function(){
					    	
					var formData = new FormData($("#photoUploadForm")[0]); 
					
					$.ajax({ 
						
						url: '<c:url value="/excel/doUpload"/>' , 
						type: 'POST', 
						data: formData, 
						enctype: 'multipart/form-data',
				        processData: false,  // Important!
				        contentType: false,
				        cache: false,
				        
						success: function (result) { 
							
							if( result.code == '0000' ){
								swal({
					                title: "成功",
					                text: result.message,
					                type: "success"
					            }, function () {
					            	$(location).attr('href', '<c:url value="/excel"/>');
					            });		
							}else{
								swal({
					                title: "失敗",
					                text: result.message,
					                type: "error"
					            }, function () {
					            	
					            });		
							}
							
						}, 
						
						error: function (result) { 
							console.log(result); 
						} 
					}); 
					
					
					
				})
					
					
				
				
				
					      
			
				
				
				
					
			});
			
		</script>
		
		<div class="row wrapper border-bottom white-bg page-heading">
		    <div class="col-lg-10">
		        <h2>excel匯入</h2>
		    </div>
		</div>
		<div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="file-manager">
                                
                                <div class="input-group mt-3">
	                                <form method="POST" enctype="multipart/form-data" id="photoUploadForm">
		                                <div class="custom-file">
				                                <input id="inputGroupFile01" type="file" class="custom-file-input" name="excelFile">
				                                <label class="custom-file-label" for="inputGroupFile01" data-browse="選擇excel">excel名稱</label>
		                                </div>
		                                <button type="button" class= "btn btn-primary btn-sm mt-2" id="excelUploadBtn" >匯入</button>
	                                </form>
                              </div>
                                
                                <div class="hr-line-dashed"></div>
                                
                                <div class="col-lg-12 animated fadeInRight">
				                    <div class="row" id ="photoCardList">
					                     <div class="col-lg-12">
							                <div class="ibox ">
							                    <div class="ibox-title">
							                        <h5>匯入的excel結果</h5>
							                        <div class="ibox-tools">
							                            <a class="collapse-link">
							                                <i class="fa fa-chevron-up"></i>
							                            </a>
							                            <a class="dropdown-toggle" data-toggle="dropdown" href="table_basic.html#">
							                                <i class="fa fa-wrench"></i>
							                            </a>
							                            <ul class="dropdown-menu dropdown-user">
							                                <li><a href="table_basic.html#" class="dropdown-item">Config option 1</a>
							                                </li>
							                                <li><a href="table_basic.html#" class="dropdown-item">Config option 2</a>
							                                </li>
							                            </ul>
							                            <a class="close-link">
							                                <i class="fa fa-times"></i>
							                            </a>
							                        </div>
							                    </div>
							                    <div class="ibox-content">
							                        <table class="table">
							                        	<thead>
								                            <tr>
								                                <th>幣別</th>
								                                <th>匯率</th>
								                                <th>現金</th>
								                                <th>即期</th>
								                                <th>遠期10天</th>
								                                <th>遠期30天</th>
								                                <th>遠期60天</th>
								                                <th>遠期90天</th>
								                                <th>遠期120天</th>
								                                <th>遠期150天</th>
								                                <th>遠期180天</th>
								                                
								                            </tr>
							                            </thead>
							                            <tbody>
							                           		<c:forEach var="item"   items="${excelData}"  >
								                           		<tr>
									                            	<td>${item.coin}</td>
									                                <td>${item.exchange}</td>
									                                <td>${item.money}</td>
									                                <td>${item.current}</td>
									                                <td>${item.tenDays}</td>
									                                <td>${item.thirtyDays}</td>
									                                <td>${item.sixtyDays}</td>
									                                <td>${item.nintyDays}</td>
									                                <td>${item.oneHundredTwentyDays}</td>
									                                <td>${item.oneHundredFiftyDays}</td>
									                                <td>${item.oneHundredEightyDays}</td> 
									                            </tr>
  														 	</c:forEach>
							                            </tbody>
							                        </table>
							
							                    </div>
							                </div>
	            						</div>   
				                    </div>
                  				</div>
                            </div>
                        </div>
                    </div>
                </div>
          	</div>
        </div>
	</body>
</html>





<!--  <div class="file-box"><div class="file"><a href="file_manager.html#"><span class="corner"></span><div class="image"><img alt="image" class="img-fluid" src="img/p1.jpg"></div> <div class="file-name">Italy street.jpg</div></a></div></div> -->
 