<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<body>
	<!-- iCheck -->
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"/>"rel="stylesheet">
		<link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/plugins/iCheck/custom.css"/>" rel="stylesheet">
		<script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/iCheck/icheck.min.js"/>"></script>
		 <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/plugins/bs-custom-file/bs-custom-file-input.min.js"/>"></script>
	
		<script type="text/javascript">
			$(function(){
				
				var photoUploadList = ${photoUploadList};
				
				
				
				
				for(var i = 0; i < photoUploadList.length; i++){
					var name = photoUploadList[i];
					var nameSubstring = null;
					if(name.length > 24){
						nameSubstring = name.substr(0,24);
					}else{
						nameSubstring = name;
					}
					$('#photoCardList').append(' <div class="file-box"><div class="file"><a title="'+name+'" href="<c:url value="/photoUpload/show/'+name+'"/>"><span class="corner"></span><div class="image"><img alt="image" class="img-fluid" src="<c:url value="/photoUpload/show/'+name+'"/>"></div> <div class="file-name">'+nameSubstring+'</div></a></div></div>');
				}
				
				
				
				
				
				
				
				bsCustomFileInput.init();
				
				
				$('#photoUploadBtn').click(function(){
					
					var fileSize = $("#photoUploadForm")[0][0].files[0].size;
				    var size = fileSize / 1024 /1024;
				    
				    if( size >= 1) {
				    	
				    	swal({
			                title: "失敗",
			                text: "圖片限制1MB ， 請重新上傳圖片",
			                type: "error"
			            });
				    	
				    	return;
				    }
				       	
					var formData = new FormData($("#photoUploadForm")[0]); 
					
					$.ajax({ 
						
						url: '<c:url value="/photoUpload/doUpload"/>' , 
						type: 'POST', 
						data: formData, 
						enctype: 'multipart/form-data',
				        processData: false,  // Important!
				        contentType: false,
				        cache: false,
				        
						success: function (result) { 
							
							if( result.code == '0000' ){
								swal({
					                title: "成功",
					                text: result.message,
					                type: "success"
					            }, function () {
					            	$(location).attr('href', '<c:url value="/photoUpload"/>');
					            });		
							}else{
								swal({
					                title: "失敗",
					                text: result.message,
					                type: "error"
					            }, function () {
					            	
					            });		
							}
							
						}, 
						
						error: function (result) { 
							console.log(result); 
						} 
					}); 
					
					
					
				})
					
					
				
				
				
					      
			
				
				
				
					
			});
			
		</script>
		
		<div class="row wrapper border-bottom white-bg page-heading">
		    <div class="col-lg-10">
		        <h2>上傳圖檔</h2>
		    </div>
		</div>
		<div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox ">
                        <div class="ibox-content">
                            <div class="file-manager">
                                
                                <div class="input-group mt-3">
	                                <form method="POST" enctype="multipart/form-data" id="photoUploadForm">
		                                <div class="custom-file">
				                                <input id="inputGroupFile01" type="file" class="custom-file-input" name="photoFile">
				                                <label class="custom-file-label" for="inputGroupFile01" data-browse="選擇圖片">圖片名稱</label>
		                                </div>
		                                <button type="button" class= "btn btn-primary btn-sm mt-2" id="photoUploadBtn" >上傳</button>
	                                </form>
                              </div>
                                
                                <div class="hr-line-dashed"></div>
                                
                                <div class="col-lg-12 animated fadeInRight">
				                    <div class="row" id ="photoCardList">
				                    
			                            <!-- <div class="file-box">
			                                <div class="file">
			                                    <a href="file_manager.html#">
			                                        <span class="corner"></span>
			
			                                        <div class="image">
			                                            <img alt="image" class="img-fluid" src="img/p2.jpg">
			                                        </div>
			                                        <div class="file-name">
			                                            My feel.png
			                                            
			                                            
			                                        </div>
			                                    </a>
			                                </div>
			                            </div>
			                            <div class="file-box">
			                                <div class="file">
			                                    <a href="file_manager.html#">
			                                        <span class="corner"></span>
			
			                                        <div class="image">
			                                            <img alt="image" class="img-fluid" src="img/p1.jpg">
			                                        </div>
			                                        <div class="file-name">
			                                            Italy street.jpg
			                                           
			                                        </div>
			                                    </a>
			                                </div>
			                            </div> -->
			                            
				                    </div>
                  				</div>
                            </div>
                        </div>
                    </div>
                </div>
          	</div>
        </div>
        <div class="footer">
            <div class="float-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2018
            </div>
        </div> 
	</body>
</html>



 <div class="file-box"><div class="file"><a href="file_manager.html#"><span class="corner"></span><div class="image"><img alt="image" class="img-fluid" src="img/p1.jpg"></div> <div class="file-name">Italy street.jpg</div></a></div></div>
 