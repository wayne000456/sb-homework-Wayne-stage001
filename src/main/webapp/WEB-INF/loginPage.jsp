<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	    <title>INSPINIA | Login</title>
	    
	    <!-- css -->
	    <link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/bootstrap.min.css"/>" rel="stylesheet">
	    <link href="<c:url value="/resources/INSPINIA-master-2.9.4/font-awesome/css/font-awesome.css"/>" rel="stylesheet">
	    <link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/animate.css"/>" rel="stylesheet">
	    <link href="<c:url value="/resources/INSPINIA-master-2.9.4/css/style.css"/>" rel="stylesheet">
	    <!-- js -->
	    <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/jquery-3.1.1.min.js"/>"></script>
	    <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/popper.min.js"/>"></script>
	    <script src="<c:url value="/resources/INSPINIA-master-2.9.4/js/bootstrap.js"/>"></script>
	</head>
	<script>
	$(function(){
		
		$("#idForm").submit(function(e) {
			
			    var form = $(this);
			    var url = form.attr('action');

			    $.ajax({
			           type: "GET",
			           url: url,
			           data: form.serialize(), // serializes the form's elements.
			           success: function(data)
			           {
			        	   console.log(data);
			        	   if(data.code == 'OK'){
			        		   $(location).prop('href', 'index');
			        	   }else {
			        		   $('#message').text(data.message);
			        	   }
			           
			           }
			         });

			    e.preventDefault(); // avoid to execute the actual submit of the form.
			});
		
		
		
		
	})
	
	</script>
	<body class="gray-bg">
		<h1 class="logo-name text-center">IMSOFT</h1>
	    <div class="middle-box text-center loginscreen animated fadeInDown">
	        <div>
	            <h3>會員系統</h3>
	            <p id="message" class="text-danger"></p>
	            <form  id="idForm" class="m-t" role="form" action="<c:url value="/doLogin"/>">
	                <div class="form-group">
	                    <input name = "username"  class="form-control" placeholder="帳號" >
	                </div>
	                <div class="form-group">
	                    <input name = "password" type="password" class="form-control" placeholder="密碼" >
	                </div>
	                <button type="submit" class="btn btn-primary block full-width m-b">登入</button>  
	            </form>
	        </div>
	    </div>
	</body>
	
</html>
