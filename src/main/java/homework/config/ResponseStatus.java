package homework.config;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum ResponseStatus {

	SUCCESS("0000","作業處理成功"),
	SQL_NULL("N000","作業處理失敗，查無資料，無法進行作業"),
	ERROR("E999","作業處理失敗，發生錯誤"),
	FILE_NULL("N001","作業處理失敗，未選擇檔案 ，請重新選擇檔案並上傳"),
	FILE_LIMIT("L001","作業處理失敗，選擇圖片大小過大 ，請重新選擇檔案並上傳");
	
	private String code;
	
	private String message;
	
	
	ResponseStatus(String code ,String message){
		this.code = code;
		this.message = message;
	}
	
	
	
	
	
	
	
	
	
	

}
