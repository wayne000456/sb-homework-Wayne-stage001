package homework.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
@Configuration
@Slf4j
public class MySiteMeshFilter extends ConfigurableSiteMeshFilter {
	
	@Value("${siteMesh.contentPath}")
	private String contentPath;
	
	@Value("${siteMesh.decoratorPath}")
	private String decoratorPath;
	
	@Value("${siteMesh.excludedPaths}")
	private String excludedPaths;
	

	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		
		log.debug("contentPath : " + contentPath);
		log.debug("decoratorPath : " + decoratorPath);
		
		String[] contentPaths = contentPath.split(",");
		for (String contentPath : contentPaths) {
			log.debug("contentPath : " + contentPath) ;
			builder.addDecoratorPath(contentPath, decoratorPath);
		}
		
		if (excludedPaths == null) {
			return;
		}
		String[] paths = excludedPaths.split(",");
		for (String path : paths) {
			builder.addExcludedPath(path);
		}
	}

}
