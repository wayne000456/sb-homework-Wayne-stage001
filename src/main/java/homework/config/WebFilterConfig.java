package homework.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import homework.dto.BaseResponse;
import homework.entity.AccountEntity;
import homework.repository.AccountRepository;

@Component
@WebFilter("/*")
public class WebFilterConfig implements Filter {
	
	@Value("${openUrl}")
	private String openUrl;
	
	@Autowired
	AccountRepository usersRepository;
	
	@Autowired
	ObjectMapper om;
	
	private final org.slf4j.Logger logger = LoggerFactory.getLogger(WebFilterConfig.class);

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		logger.info("start");
		
		HttpServletRequest httpReq = (HttpServletRequest) request;
		HttpServletResponse httpRes = (HttpServletResponse) response;
		
		String url = httpReq.getRequestURI();
		logger.info("進來的url : {}" ,url);
		
		String[] urls = openUrl.split(",");
		List<String> urlItems = Arrays.asList(urls);
		
		AccountEntity usersEntity = (AccountEntity)httpReq.getSession().getAttribute("User");
		
		
		
		logger.info("sessionId : {}",httpReq.getRequestedSessionId());
		
		logger.info("sessionCreateTime : {}", new Date(httpReq.getSession().getCreationTime()));
		
		if(usersEntity != null) {
			logger.info("sessionUser : {}", usersEntity.getName());	
		}
		
		
		if ( url.contains("/doLogin") ) {
			
			logger.info("doLogin");
			
			BaseResponse baseResponse = new BaseResponse();
			
			httpRes.setCharacterEncoding("UTF-8");
	 		httpRes.setContentType("application/json");
	 		
			String username = httpReq.getParameter("username");
			String password = httpReq.getParameter("password");
			
			Optional<AccountEntity> userEntityOpt = usersRepository.findByUsernameAndPassword(username, password);
			
			if ( userEntityOpt.isPresent() ) {
				
				logger.info("帳密正確 準備檢查狀態 ");
			 	AccountEntity usersDeail = userEntityOpt.get(); 
			 	
			 	if( usersDeail.getStatus() == null) {
			 		
			 		logger.info("登入成功");
				 	usersDeail.setStatus("T");
				 	usersRepository.save(usersDeail);
				 	httpReq.getSession().setAttribute("User",usersDeail);
				 	
				 	baseResponse.setCode("OK");
			 		
			 		String json = om.writeValueAsString(baseResponse);
			 		httpRes.getWriter().print(json);
			 		
				 	return;
			 	}else {
			 		
			 		logger.info("帳號目前已經登入中 ， 請稍後再試");
			 		
			 		baseResponse.setCode("ONLINE");
			 		baseResponse.setMessage("帳號目前已經登入中 ， 請稍後再試");
			 		
			 		String json = om.writeValueAsString(baseResponse);
			 		httpRes.getWriter().print(json);
			 		//chain.doFilter(request, response);
					return ;
			 		
			 	}
			}else {
				logger.info("登入失敗 ， 帳密錯誤 ");
				
				baseResponse.setCode("NOUSER");
		 		baseResponse.setMessage("帳號或密碼錯誤");
		 		
		 		String json = om.writeValueAsString(baseResponse);
		 		httpRes.getWriter().print(json);
				//httpRes.sendRedirect("login");
				return ;
			}
			
		} 
		
		if ( url.contains("/doLogout") ) {	
			
			logger.info("doLogout");
			
	    	if( usersEntity != null  ) {	
	    		logger.info("已登入 進行刪除");
	    		AccountEntity cccountEntity = usersRepository.findById(usersEntity.getUsername()).get();
	    		cccountEntity.setStatus(null);
	    		usersRepository.save(cccountEntity);
	    		httpReq.getSession().removeAttribute("User");
	    		httpRes.sendRedirect("login");
				return ;
	    		
	    	}
		} 
			
		for ( String item : urlItems ) {
			logger.info("  {}  比對{} 結果{} ",url,item,url.contains(item));
		    
			if(url.contains(item)) {
				logger.info("{} 不需要登入",url);
				chain.doFilter(request, response);
				return ;
			}
		}
		
		if( usersEntity != null  ) {
    		logger.info("已登入 直接顯示畫面");
    		logger.info("testtttttt :{} ",usersEntity);
    		chain.doFilter(request, response);
    	}else {
    		logger.info("未登入 踢回登入頁");
    		httpRes.sendRedirect(httpReq.getContextPath()+"/login");
    	}
		
	}
	
}
