package homework.contorller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainContorller {
	
	
	@GetMapping("/index")
	public ModelAndView index() {
		
		return new ModelAndView("index");
		
	}
	
	@GetMapping("/login")
	public ModelAndView login() {
		
		return new ModelAndView("loginPage");
		
	}
	
		

}
