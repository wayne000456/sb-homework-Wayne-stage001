package homework.contorller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import homework.config.MyException;
import homework.config.ResponseStatus;
import homework.dto.AccountRequest;
import homework.dto.BaseResponse;
import homework.dto.DoUserSaveRequest;
import homework.dto.DoUsersDeleteRequest;
import homework.dto.TableResponse;
import homework.entity.AccountEntity;
import homework.repository.AccountRepository;
import homework.service.ProfileService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/profile")
@Slf4j
public class ProfileController {
	
	@Autowired
	AccountRepository usersRepository;
	
	@Autowired 
	ProfileService profileService;
	
	@GetMapping("")
	public ModelAndView profile() {
		
		return new ModelAndView("profile/index");
		
	}
	
	@GetMapping("createAccount")
	public ModelAndView createAccount() {
		
		return new ModelAndView("profile/accountEdit");
		
	}
	
	@GetMapping("editAccount/{username}")
	public ModelAndView editAccount(@PathVariable String username ) throws Exception {
	
		
		AccountEntity usersEntity = profileService.editAccount(username);
		
		ModelAndView modelAndView = new ModelAndView("profile/accountEdit");
		
		modelAndView.addObject("accountDetail",usersEntity);
		
		return modelAndView;
		
	}
	
	
	
	
	@PostMapping("doAccountCreate")
	@ResponseBody
	public BaseResponse doAccountCreate(@RequestBody AccountRequest accountRequest ) {
		
		log.info("{}",accountRequest);
		
		BaseResponse baseResponse = new BaseResponse();
	
		try {
			
			profileService.doAccountCreate(accountRequest);
			
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		} catch (MyException e) {
			//帳號已存在
			baseResponse.setCode("E0002");
			baseResponse.setMessage(e.getMessage());
		
		} catch (Exception e) {
			
			log.error("發生錯誤 : {} ",e);
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
			
		}
		
		return baseResponse;
		
		
	}
	
	
	@PostMapping("doAccountSave")
	@ResponseBody
	public BaseResponse doAccountSave(@RequestBody AccountRequest accountRequest ,HttpServletRequest httpServletRequest ) {
		
		
		log.info("{}",accountRequest);
		
		BaseResponse baseResponse = new BaseResponse();
	
		try {
			
			profileService.doAccountSave(accountRequest);
			
			httpServletRequest.getSession().setAttribute("User",usersRepository.findById(accountRequest.getUsername()).get());
			
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		} catch (MyException e) {
			
			baseResponse.setCode(ResponseStatus.SQL_NULL.getCode());
			baseResponse.setMessage(ResponseStatus.SQL_NULL.getMessage());
			
		}catch (Exception e) {
			
			log.error("發生錯誤 : {} ",e);
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
			
		}
		
		return baseResponse;
		
		
	}
	
	
	
	
	@PostMapping("userTable")
	@ResponseBody
	public TableResponse userTable() {
		
		List<AccountEntity> usersEntityList = usersRepository.findAll();
		
		TableResponse tableResponse = new TableResponse();
		
		tableResponse.setData(usersEntityList);
		
		return tableResponse;
		
		
	}
	
	@PostMapping("doUserSave")
	@ResponseBody
	public BaseResponse doUserSave(@RequestBody DoUserSaveRequest doUserSaveRequest ) {
		
		log.info("{}",doUserSaveRequest);
		
		BaseResponse baseResponse = new BaseResponse();
		
		try {
			
			profileService.doUserSave(doUserSaveRequest);
			
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		} catch (MyException e) {
			
			baseResponse.setCode(ResponseStatus.SQL_NULL.getCode());
			baseResponse.setMessage(ResponseStatus.SQL_NULL.getMessage());
		
		} catch (Exception e) {
			
			log.error("發生錯誤 : {} ",e);
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
			
		}
		
		return baseResponse;
		
	}
	
	
	@PostMapping("doUsersDelete")
	@ResponseBody
	public BaseResponse doUsersDelete(@RequestBody DoUsersDeleteRequest doUsersDeleteRequest) {
		
		log.info("{}",doUsersDeleteRequest);
		
		BaseResponse baseResponse = new BaseResponse();
		
		try {
			
			profileService.doUsersDelete(doUsersDeleteRequest);
			
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		} catch (MyException e) {
			
			baseResponse.setCode(ResponseStatus.SQL_NULL.getCode());
			baseResponse.setMessage(e.getMessage());
		
		} catch (Exception e) {
			
			log.error("發生錯誤 : {} ",e);
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
			
		}
		
		return baseResponse;
		
	}
	
	@GetMapping(value= {"/userPhoto/{fileName}","userPhoto"})
	public void photo(@PathVariable(required = false) String fileName, HttpServletResponse response) {
		
		log.info("fileName : " + fileName);
		
		FileInputStream is =null;
		
		try {
			
			String fullName = "upload/userPhoto/"+ fileName ;
			if(fileName != null && new File(fullName).exists()) {
				
				is = new FileInputStream(new File(fullName));
				IOUtils.copy(is, response.getOutputStream());
				
			} else {
				
				is = new FileInputStream(new File("upload/userPhoto/"+"noImage.jpeg"));
				IOUtils.copy(is, response.getOutputStream());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( is != null) {
					
					is.close();
				}
				
			} catch (IOException e) {
			
				e.printStackTrace();
			}			
		}
		
	}
	


}
