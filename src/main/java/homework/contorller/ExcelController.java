package homework.contorller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import homework.config.MyException;
import homework.config.ResponseStatus;
import homework.dto.BaseResponse;
import homework.entity.ExcelEntity;
import homework.repository.ExcelRepository;
import homework.service.ExcelService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/excel")
public class ExcelController {
	
	@Autowired
	ExcelRepository excelRepository;
	
	@Autowired
	ExcelService excelService;
	
	@GetMapping("")
	public ModelAndView excel()  {
		
		ModelAndView modelAndView = new ModelAndView("excelUpload/index");
		
		List<ExcelEntity> excelEntityList = excelRepository.selectAll();
		modelAndView.addObject("excelData",excelEntityList);
		
		return modelAndView;
		
	}
	
	@PostMapping("doUpload")
	@ResponseBody
	public BaseResponse doUpload( @RequestParam("excelFile") MultipartFile excelFile ){
		
		BaseResponse baseResponse = new BaseResponse();
		
		try {
			
			excelService.excelUpload(excelFile);
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		}catch (MyException e) {
			
			baseResponse.setCode(ResponseStatus.FILE_NULL.getCode());
			baseResponse.setMessage(e.getMessage());
			
		}catch (Exception e) {
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
			
		}
		
		return baseResponse;
		
		
	}
	
	

}
