package homework.contorller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import homework.config.MyException;
import homework.config.ResponseStatus;
import homework.dto.BaseResponse;
import homework.dto.FileNameListVo;
import homework.dto.FileNameListVo.FileName;
import homework.service.PhotoUploadService;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/photoUpload")
@Slf4j
public class PhotoUploadController {
	
	@Autowired
	PhotoUploadService photoUploadService;
	
	@Autowired
	ObjectMapper om;
	
	/**顯示畫面 photoUpload/index
	 * 查詢圖片名稱
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping("")
	public ModelAndView photoUpload() throws JsonProcessingException {
		
		List<String> fileNameList = photoUploadService.getImageNames();
		
		String fileNameListJson = om.writeValueAsString(fileNameList);
		
		ModelAndView modelAndView = new ModelAndView("photoUpload/index");
		
		modelAndView.addObject("photoUploadList", fileNameListJson);
		
		return modelAndView;
		
	}
	
	
	/**上傳圖片
	 * 
	 * @param photpFile
	 * @return BaseResponse
	 */
	@PostMapping("doUpload")
	@ResponseBody
	public BaseResponse doUpload( @RequestParam("photoFile") MultipartFile photpFile ) {
		
		BaseResponse baseResponse = new BaseResponse();
		
		try {
			
			photoUploadService.doUpload(photpFile);
			
			baseResponse.setCode(ResponseStatus.SUCCESS.getCode());
			baseResponse.setMessage(ResponseStatus.SUCCESS.getMessage());
			
		} catch (MyException e) {
			
			baseResponse.setCode(ResponseStatus.FILE_NULL.getCode());
			baseResponse.setMessage(ResponseStatus.FILE_NULL.getMessage());
			
		}
		catch (Exception e) {
			
			baseResponse.setCode(ResponseStatus.ERROR.getCode());
			baseResponse.setMessage(ResponseStatus.ERROR.getMessage());
		}
	
		return baseResponse;
		
	}
	
	/** url 顯示圖片
	 * 
	 * @param fileName
	 * @param response
	 * @throws Exception
	 */
	@GetMapping(value= {"/show/{fileName}"})
	public void show(@PathVariable String fileName, HttpServletResponse response) throws Exception {
		
		log.info("fileName : " + fileName);
		
		photoUploadService.show(fileName, response);
		
		
	}
	
	

}
