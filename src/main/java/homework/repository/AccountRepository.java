package homework.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import homework.entity.AccountEntity;

public interface AccountRepository extends JpaRepository<AccountEntity, String>{
	
	Optional<AccountEntity> findByUsernameAndPassword(String username,String password);

}
