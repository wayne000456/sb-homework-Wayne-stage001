package homework.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import homework.entity.ExcelEntity;

@Transactional
public interface ExcelRepository extends JpaRepository<ExcelEntity, String>{
	
	
	@Modifying 
	@Query(value="INSERT INTO EXCEL VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11);",nativeQuery = true)
	void insert ( String coin , String exchange ,String money, String current , String tenDays, String thirtyDays, String sixtyDays, String nintyDays, String oneHundredTwentyDays, String oneHundredFiftyDays, String oneHundredEightyDays );

	@Query(value="SELECT * FROM EXCEL",nativeQuery = true)
	List<ExcelEntity> selectAll();
}
