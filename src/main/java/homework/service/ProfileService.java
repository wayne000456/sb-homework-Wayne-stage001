package homework.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import homework.config.MyException;
import homework.dto.AccountRequest;
import homework.dto.DoUserSaveRequest;
import homework.dto.DoUsersDeleteRequest;
import homework.entity.AccountEntity;
import homework.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProfileService {
	
	@Autowired
	AccountRepository usersRepository;
	
	@Autowired
	HttpServletRequest httpServletRequest;
	
	
	public void doUserSave(DoUserSaveRequest doUserSaveRequest ) throws Exception {
		
		Optional<AccountEntity> usersEntityOpt = usersRepository.findById(doUserSaveRequest.getUsername());
		
		if(usersEntityOpt.isPresent()) {
			
			AccountEntity usersEntity = usersEntityOpt.get();
			usersEntity.setName(doUserSaveRequest.getName());
			usersEntity.setPhone(doUserSaveRequest.getPhone());
			usersEntity.setEmail(doUserSaveRequest.getEmail());
			usersEntity.setInformation(doUserSaveRequest.getImformation());
			
			usersRepository.save(usersEntity);
			
		}else {
			log.error("username : {} 查詢為空 無法進行 doUserSave 儲存更新使用者資訊",doUserSaveRequest.getUsername());
			throw new MyException("查詢為空 無法進行 doUserSave");
		}
		
		
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void doUsersDelete(DoUsersDeleteRequest doUsersDeleteRequest ) throws Exception {
		
		List<String> delUsersList = doUsersDeleteRequest.getDelUsersList();
		
		for( String username : delUsersList ) {
			Optional<AccountEntity> usersEntityOpt = usersRepository.findById(username);
			if( usersEntityOpt.isEmpty() ) {
				log.error("username : {} 查詢為空 無法進行 doUsersDelete 刪除使用者資訊",username);
				throw new MyException("刪除作業處理失敗，其中有帳號發生問題  ");
			}else {
				usersRepository.deleteById(username);
			}
		}
		
		
	}
	
	
	public void doAccountSave( AccountRequest accountRequest ) throws Exception {
	
		Optional<AccountEntity> usersEntityOpt = usersRepository.findById( accountRequest.getUsername() );
		
		if( usersEntityOpt.isPresent() ) {
			
			String imageUrl = saveAccountPoto(accountRequest.getImage());
			
			AccountEntity usersEntity = usersEntityOpt.get();
			usersEntity.setName(accountRequest.getName());
			usersEntity.setPhone(accountRequest.getPhone());
			usersEntity.setEmail(accountRequest.getEmail());
			usersEntity.setInformation(accountRequest.getInformation());

			if( imageUrl != null) {
				usersEntity.setImage(imageUrl);
			}
		
			usersRepository.save(usersEntity);
		}else {
			log.error("username : {} 查詢為空 無法進行 doAccountSave 儲存更新帳戶資訊", accountRequest.getUsername() );
			throw new MyException("查詢為空 無法進行 doAccountSave");
			
		}	
		
	}
	
	
	public void doAccountCreate( AccountRequest accountRequest ) throws Exception {
		
		Optional<AccountEntity> usersEntityOpt = usersRepository.findById( accountRequest.getUsername() );
		
		if( usersEntityOpt.isEmpty() ) {
			
			String imageUrl = saveAccountPoto(accountRequest.getImage());
			
			AccountEntity usersEntity = new AccountEntity();
			usersEntity.setUsername(accountRequest.getUsername());
			usersEntity.setPassword(accountRequest.getPassword());
			usersEntity.setName(accountRequest.getName());
			usersEntity.setPhone(accountRequest.getPhone());
			usersEntity.setEmail(accountRequest.getEmail());
			usersEntity.setInformation(accountRequest.getInformation());
			
			if( imageUrl != null) {
				usersEntity.setImage(imageUrl);
			}
			
			usersRepository.save(usersEntity);
			
		}else {
			log.error("username : {} 查詢已存在 無法進行 doAccountCreate 建立帳戶資訊", accountRequest.getUsername() );
			throw new MyException("此帳號已存在 ，請重新輸入帳號");
			
		}
		
		
		
		
	}
	
	public AccountEntity editAccount(String username ) throws Exception {
		
		
		
		Optional<AccountEntity> usersEntityOpt = usersRepository.findById(username);
		
		if( usersEntityOpt.isPresent() ) {
			
			return usersEntityOpt.get();
		}else {
			log.error("username : {} 查詢無 無法進行 editAccount 編輯個人帳戶資訊", username );
			throw new MyException("查詢無 無法進行 editAccount 編輯個人帳戶資訊");
		}
	}
	
	
	private String saveAccountPoto( String image  ) throws Exception {
		
		//比對格式 1.為 base64 > 加工為url  2.為url 不加工 直接回傳url 3.都不是 回傳null
				
		if ( image.contains(";base64,") ) {
			
			log.info("收到圖片 為 base64 格式");
			
			image= image.substring(image.indexOf(",")+1);
			
		}else if (image.contains("http://")) {
			
			log.info("收到圖片 為 url 格式");
			
			return null;		
		}else {
			
			log.error("收到圖片 不符合 格式");
			
			throw new Exception("收到圖片 不符合 格式") ;
		}
				
		InputStream is = null;
		FileImageOutputStream os = null;
				
		try {
				//圖片用的uuid當名稱
				String uuid= UUID.randomUUID().toString();

				//base 64 解碼後 轉 byte
				byte[] decodedBytes = Base64.getDecoder().decode(image.getBytes());
				
				is = new ByteArrayInputStream(decodedBytes);
				
				//mimeType is something like "image/jpeg" <取出左邊格式 包含副檔名
				String mimeType = URLConnection.guessContentTypeFromStream(is); 
				
				//取出副檔名並加上.
				String fileExtension = "."+mimeType.substring(mimeType.lastIndexOf("/")+1);
				
		        //產生檔案及賦予副檔名
			    os = new FileImageOutputStream( new File("upload" +"/userPhoto" + "/" +  uuid + fileExtension ) );
			    os.write(decodedBytes);
				
			    String fullName =  uuid + fileExtension;
			    
			    log.info("產生 檔名 {}" , fullName);
			  
				return fullName;
				
		}catch (Exception e) {
			
			e.printStackTrace();
			throw e;	
			
		}finally {
				
			//關閉inputStream 與 outputStream
			try {
				
				if (is != null) {
					is.close();
				}
				if (os != null) {
					os.close();
				}
				
			}catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
				
				
		}
		
//				String contextPath = httpServletRequest.getContextPath();
//				String localName = httpServletRequest.getLocalName();
//				String localPort = Integer.toString(httpServletRequest.getLocalPort());
//				
//				//http://localhost:8888/IMSOFT
//				String urlDomain = "http://" + localName + ":" + localPort + contextPath;
		
		
	}

}
