package homework.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.compress.utils.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import homework.config.MyException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PhotoUploadService {
	
	public List<String> getImageNames() {
		
		List<String> fileNameList = new ArrayList<>();
		
		File path = new File("upload/photo");	
		File[] files  = path.listFiles();
		
		if( files != null ) {
			for ( File file : files) {
				log.info("圖片名稱 : {}",file.getName());
				fileNameList.add(file.getName());
			}
		}
		
		return fileNameList;
		
	}

	public void doUpload(MultipartFile photpFile) throws Exception {

		log.info("{}", photpFile);

		if (photpFile.isEmpty()) {

			log.error("未選擇圖片 ，請重新上傳圖片");
			throw new MyException("未選擇圖片 ，請重新上傳圖片");

		} else {

			String fileName = photpFile.getOriginalFilename();
			photpFile.transferTo(new File(new File("upload/photo").getAbsolutePath() + "/" + fileName));

		}

	}

	public void show(String fileName, HttpServletResponse response) throws Exception {

		FileInputStream is = null;

		try {

			String fullName = "upload/photo/" + fileName;
			if (fileName != null && new File(fullName).exists()) {

				is = new FileInputStream(new File(fullName));
				IOUtils.copy(is, response.getOutputStream());

			} else {

				is = new FileInputStream(new File("upload/photo/" + "no-image.png"));
				IOUtils.copy(is, response.getOutputStream());
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error(" show ()  io 發生錯誤");
			throw e ;
			
		} finally {
			
			try {
				if(is != null) {
					is.close();
				}
			} catch (IOException e) {
				log.error(" show ()  io關閉 發生錯誤");
			}
			
		}

	}
}
