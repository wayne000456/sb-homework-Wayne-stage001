package homework.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import homework.config.MyException;
import homework.entity.ExcelEntity;
import homework.repository.ExcelRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExcelService {
	
	@Autowired
	ExcelRepository excelRepository;
	
	public void excelUpload( MultipartFile excelFile ) throws Exception {
		
		
		if( !excelFile.isEmpty() ) {
			
			excelRepository.deleteAll();
					
			HSSFWorkbook workbook = new HSSFWorkbook(excelFile.getInputStream());
			HSSFSheet sheet = workbook.getSheetAt(0);
			
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				
				HSSFCell coin = sheet.getRow(i).getCell(0);
				
				List<String> list = new ArrayList<>();
				list.add(coin.toString());
				for (int j = 1; j <= 10 ; j++) {
					HSSFCell data = sheet.getRow(i).getCell(j);
					list.add(data.toString());
				}
				log.info("list : {}" ,list);
				ExcelEntity excelEntity = new ExcelEntity();
				excelEntity.setCoin(list.get(0));
				excelEntity.setExchange(list.get(1));
				excelEntity.setMoney(list.get(2));
				excelEntity.setCurrent(list.get(3));
				excelEntity.setTenDays(list.get(4));
				excelEntity.setThirtyDays(list.get(5));
				excelEntity.setSixtyDays(list.get(6));
				excelEntity.setNintyDays(list.get(7));
				excelEntity.setOneHundredTwentyDays(list.get(8));
				excelEntity.setOneHundredFiftyDays(list.get(9));
				excelEntity.setOneHundredEightyDays(list.get(10));
				excelRepository.save(excelEntity);
				//excelRepository.insert(list.get(0),list.get(1), list.get(2), list.get(3), list.get(4), list.get(5), list.get(6), list.get(7), list.get(8), list.get(9), list.get(10));
				
				list.clear();
				list.add(coin.toString());
				for (int j = 11 ; j <= 20; j++) {
					HSSFCell data = sheet.getRow(i).getCell(j);
					list.add(data.toString());
				}
				log.info("清空後 第二個list : {}" ,list);
				ExcelEntity excelEntity2 = new ExcelEntity();
				excelEntity2.setCoin(list.get(0));
				excelEntity2.setExchange(list.get(1));
				excelEntity2.setMoney(list.get(2));
				excelEntity2.setCurrent(list.get(3));
				excelEntity2.setTenDays(list.get(4));
				excelEntity2.setThirtyDays(list.get(5));
				excelEntity2.setSixtyDays(list.get(6));
				excelEntity2.setNintyDays(list.get(7));
				excelEntity2.setOneHundredTwentyDays(list.get(8));
				excelEntity2.setOneHundredFiftyDays(list.get(9));
				excelEntity2.setOneHundredEightyDays(list.get(10));
				excelRepository.save(excelEntity2);
				//excelRepository.insert(list.get(0),list.get(1), list.get(2), list.get(3), list.get(4), list.get(5), list.get(6), list.get(7), list.get(8), list.get(9), list.get(10));
				
			}
					
		}else {
			log.error(" 收到檔案為空 未上傳excel");
			throw new MyException("未選擇excel，請重新選擇excel並上傳");
			
		}
					
	}

}
