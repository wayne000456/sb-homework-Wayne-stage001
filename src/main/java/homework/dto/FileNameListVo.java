package homework.dto;

import java.util.List;

import lombok.Data;

@Data
public class FileNameListVo {

	private List<FileName> fileNameList;

	@Data
	public static class FileName {

		private String theFileName;

	}

}
