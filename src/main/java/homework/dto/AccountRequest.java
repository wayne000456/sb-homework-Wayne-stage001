package homework.dto;

import lombok.Data;

@Data
public class AccountRequest {
	
	
	private String username ;
	
	private String password ;
	
	private String name ;
	
	private String phone ;
	
	private String email ;
	
	private String information ;
	
	private String image;

}
