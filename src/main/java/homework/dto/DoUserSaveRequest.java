package homework.dto;

import lombok.Data;

@Data
public class DoUserSaveRequest {
	
	private String username;
	
	private String name;
	
	private String phone;
	
	private String email;
	
	private String imformation;

}
