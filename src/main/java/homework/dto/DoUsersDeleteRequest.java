package homework.dto;

import java.util.List;

import lombok.Data;
@Data
public class DoUsersDeleteRequest {
	
	List<String> delUsersList;

}
