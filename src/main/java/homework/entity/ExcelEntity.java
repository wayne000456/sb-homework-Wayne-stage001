package homework.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="EXCEL")
@Data
public class ExcelEntity {
	
	
	@Id 
	@SequenceGenerator( name = "SEQ_EXCEl", sequenceName = "SEQ_EXCEl", allocationSize = 1, initialValue = 1 )
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "SEQ_EXCEl")
	private Integer id;

	private String coin;
	
	private String exchange;
	
	private String money;
	
	private String current; 
	
	private String tenDays;
	
	private String thirtyDays;
	
	private String sixtyDays;
	
	private String nintyDays;  
	
	private String oneHundredTwentyDays; 
	
	private String oneHundredFiftyDays; 
	
	private String oneHundredEightyDays;

	

}
