package homework.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="ACCOUNT")
@Data
public class AccountEntity implements Serializable{
	
	private static final long serialVersionUID = 5055584547267744666L;
	@Id
	private String username;
	
	private String password;
	
	private String name;
	
	private String phone;
	
	private String email;
	
	private String information;
	
	private String image;
	
	private String status;
	
	

}
